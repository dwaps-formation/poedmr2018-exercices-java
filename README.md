![](https://www.dwaps.fr/img/logo-300.png)

# FORMATION ANDROID
L'entreprise **DWAPS Formation** a été créée le 1er Octobre 2015.

Sa vocation est de former des développeurs web et mobile.

Rendez-vous sur le site **[dwaps.fr](http://dwaps.fr "DWAPS")** pour plus d'informations.

---

# Installation
## 1. Récupération du code des modules

  Choisissez un emplacement sur votre ordinateur et saisissez la commande suivante dans un terminal :
    
  `git clone https://bitbucket.org/dwaps-formation/poedmr2018-exercices-java`

## Se balader dans les exercices

  `git checkout 1` par exemple pour afficher le tout premier exercice.

---

[® DWAPS Formation - Michael Cornillon] (http://dwaps.fr "DWAPS")
