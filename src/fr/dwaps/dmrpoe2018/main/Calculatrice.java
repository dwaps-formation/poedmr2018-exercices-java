package fr.dwaps.dmrpoe2018.main;

import java.util.Scanner;

public class Calculatrice {
	public void start(Scanner sc) {
		String input;
		String[] inputTab;
		long nb1 = 0, nb2 = 0;
		Long result = null;
		
		while (true) {
			while (true) {
				System.out.print("Entrez votre opération : ");
				input = sc.nextLine();
				inputTab = input.split("");
				
				try {
					nb1 = Long.parseLong(inputTab[0]);
					nb2 = Long.parseLong(inputTab[2]);
					
					break;
				}
				catch (Exception e) {
					System.out.println("Oups, y'a un soucis là !");
				}
			}
			
			switch (inputTab[1]) {
				case "+":
					result = nb1 + nb2;
					break;
				case "-":
					result = nb1 - nb2;
					break;
				case "/":
					result = nb1 / nb2;
					break;
				case "*":
					result = nb1 * nb2;
					break;
				case "%":
					result = nb1 % nb2;
					break;
			}
			
			System.out.print("Résultat : " + ((result != null)? result : "undefined"));
			
			System.out.print("\nUn autre calcul peut être ? (On) ");
			if (sc.nextLine().equalsIgnoreCase("n")) {
				break;
			}
			
			result = null;
		}
		
		System.out.println("A bientôt ! :)");
	}
}
