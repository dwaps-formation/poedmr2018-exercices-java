package fr.dwaps.dmrpoe2018.main;

import java.util.Scanner;

public class ExercicesJava {
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		new Calculatrice().start(sc);
		sc.close();
	}
}
